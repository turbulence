/*
 *  liquidity/vapi/gl-liquidity.vapi
 *
 *  Copyright (c) 2008 Patrick Walton <pcwalton@uchicago.edu>
 */

[CCode(cprefix="gl", lower_case_cprefix="gl")]
namespace OpenGL {
    [CCode(cname="glGenTextures", cheader_filename="GL/gl.h")]
    public static void glGenTextures(int n, uint *textures);
    [CCode(cname="glGetError", cheader_filename="GL/gl.h")]
    public static int glGetError();
    [CCode(cname="GL_TEXTURE_2D", cheader_filename="GL/gl.h")]
    public const int GL_TEXTURE_2D;
}

