/*
 *  liquidity/vapi/clutter-liquidity-0.6.vapi
 *
 *  Copyright (c) 2008 OpenedHand, Patrick Walton
 */ 

[CCode (cprefix = "Clutter", lower_case_cprefix = "clutter_")]
namespace Clutter {
	[CCode (cprefix = "CLUTTER_", cheader_filename = "clutter/clutter.h")]
	public enum EventType {
		NOTHING,
		KEY_PRESS,
		KEY_RELEASE,
		MOTION,
		ENTER,
		LEAVE,
		BUTTON_PRESS,
		BUTTON_RELEASE,
		SCROLL,
		STAGE_STATE,
		DESTROY_NOTIFY,
		CLIENT_MESSAGE,
		DELETE,
	}
	[CCode (cprefix = "CLUTTER_GRAVITY_", cheader_filename = "clutter/clutter.h")]
	public enum Gravity {
		NONE,
		NORTH,
		NORTH_EAST,
		EAST,
		SOUTH_EAST,
		SOUTH,
		SOUTH_WEST,
		WEST,
		NORTH_WEST,
		CENTER,
	}
	[CCode (cprefix = "CLUTTER_INIT_", cheader_filename = "clutter/clutter.h")]
	public enum InitError {
		SUCCESS,
		ERROR_UNKNOWN,
		ERROR_THREADS,
		ERROR_BACKEND,
		ERROR_INTERNAL,
	}
	[CCode (cprefix = "CLUTTER_", cheader_filename = "clutter/clutter.h")]
	public enum RotateAxis {
		X_AXIS,
		Y_AXIS,
		Z_AXIS,
	}
	[CCode (cprefix = "CLUTTER_ROTATE_", cheader_filename = "clutter/clutter.h")]
	public enum RotateDirection {
		CW,
		CCW,
	}
	[CCode (cprefix = "CLUTTER_SCRIPT_ERROR_INVALID_", cheader_filename = "clutter/clutter.h")]
	public enum ScriptError {
		TYPE_FUNCTION,
		PROPERTY,
		VALUE,
	}
	[CCode (cprefix = "CLUTTER_SCROLL_", cheader_filename = "clutter/clutter.h")]
	public enum ScrollDirection {
		UP,
		DOWN,
		LEFT,
		RIGHT,
	}
	[CCode (cprefix = "CLUTTER_SHADER_ERROR_", cheader_filename = "clutter/clutter.h")]
	public enum ShaderError {
		NO_ASM,
		NO_GLSL,
		COMPILE,
	}
	[CCode (cprefix = "CLUTTER_TEXTURE_ERROR_", cheader_filename = "clutter/clutter.h")]
	public enum TextureError {
		OUT_OF_MEMORY,
		NO_YUV,
	}
	[CCode (cprefix = "CLUTTER_TIMELINE_", cheader_filename = "clutter/clutter.h")]
	public enum TimelineDirection {
		FORWARD,
		BACKWARD,
	}
	[CCode (cprefix = "CLUTTER_ACTOR_", cheader_filename = "clutter/clutter.h")]
	[Flags]
	public enum ActorFlags {
		MAPPED,
		REALIZED,
		REACTIVE,
	}
	[CCode (cprefix = "CLUTTER_EVENT_FLAG_", cheader_filename = "clutter/clutter.h")]
	[Flags]
	public enum EventFlags {
		SYNTHETIC,
	}
	[CCode (cprefix = "CLUTTER_FEATURE_", cheader_filename = "clutter/clutter.h")]
	[Flags]
	public enum FeatureFlags {
		TEXTURE_RECTANGLE,
		SYNC_TO_VBLANK,
		TEXTURE_YUV,
		TEXTURE_READ_PIXELS,
		STAGE_STATIC,
		STAGE_USER_RESIZE,
		STAGE_CURSOR,
		SHADERS_GLSL,
		OFFSCREEN,
	}
	[CCode (cprefix = "CLUTTER_", cheader_filename = "clutter/clutter.h")]
	[Flags]
	public enum ModifierType {
		SHIFT_MASK,
		LOCK_MASK,
		CONTROL_MASK,
		MOD1_MASK,
		MOD2_MASK,
		MOD3_MASK,
		MOD4_MASK,
		MOD5_MASK,
		BUTTON1_MASK,
		BUTTON2_MASK,
		BUTTON3_MASK,
		BUTTON4_MASK,
		BUTTON5_MASK,
	}
	[CCode (cprefix = "CLUTTER_STAGE_STATE_", cheader_filename = "clutter/clutter.h")]
	[Flags]
	public enum StageState {
		FULLSCREEN,
		OFFSCREEN,
		ACTIVATED,
	}
	[CCode (cprefix = "CLUTTER_TEXTURE_", cheader_filename = "clutter/clutter.h")]
	[Flags]
	public enum TextureFlags {
		RGB_FLAG_BGR,
		RGB_FLAG_PREMULT,
		YUV_FLAG_YUV2,
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class Actor : GLib.InitiallyUnowned, Clutter.Scriptable {
		public uint flags;
		// [CCode (cname = "CLUTTER_ACTOR_FLAGS")]
		// public Clutter.ActorFlags get_flags ();
        [CCode(cname="CLUTTER_ACTOR_IS_REALIZED")]
        public bool is_realized();
		[CCode (cname = "CLUTTER_ACTOR_SET_FLAGS")]
		public void set_flags (Clutter.ActorFlags flags);
		[CCode (cname = "CLUTTER_ACTOR_UNSET_FLAGS")]
		public void unset_flags (Clutter.ActorFlags flags);
		public Actor ();
		public void apply_relative_transform_to_point (Clutter.Actor ancestor, Clutter.Vertex point, Clutter.Vertex vertex);
		public void apply_transform_to_point (Clutter.Vertex point, Clutter.Vertex vertex);
		public void get_abs_position (out int x, out int y);
		public void get_abs_size (out uint width, out uint height);
		public void get_anchor_point (out int anchor_x, out int anchor_y);
		public void get_anchor_pointu (out Clutter.Unit anchor_x, out Clutter.Unit anchor_y);
		public void get_clip (out int xoff, out int yoff, out int width, out int height);
		public void get_clipu (out Clutter.Unit xoff, out Clutter.Unit yoff, out Clutter.Unit width, out Clutter.Unit height);
		public void get_coords (out int x_1, out int y_1, out int x_2, out int y_2);
		public int get_depth ();
		public Clutter.Unit get_depthu ();
		public void get_geometry (Clutter.Geometry geometry);
		public uint get_gid ();
		public uint get_height ();
		public Clutter.Unit get_heightu ();
		public weak string get_name ();
		public uchar get_opacity ();
		public weak Clutter.Actor get_parent ();
		public void get_position (out int x, out int y);
		public void get_positionu (out Clutter.Unit x, out Clutter.Unit y);
		public bool get_reactive ();
		public void get_relative_vertices (Clutter.Actor ancestor, Clutter.Vertex[] verts);
		public double get_rotation (Clutter.RotateAxis axis, out int x, out int y, out int z);
		public Clutter.Fixed get_rotationx (Clutter.RotateAxis axis, int x, int y, int z);
		public void get_scale (out double scale_x, out double scale_y);
		public void get_scalex (Clutter.Fixed scale_x, Clutter.Fixed scale_y);
		public weak Clutter.Shader get_shader ();
		public void get_size (out uint width, out uint height);
		public void get_sizeu (out Clutter.Unit width, out Clutter.Unit height);
		public void get_vertices (out weak Clutter.Vertex[] verts);
		public uint get_width ();
		public Clutter.Unit get_widthu ();
		public int get_x ();
		public Clutter.Unit get_xu ();
		public int get_y ();
		public Clutter.Unit get_yu ();
		[CCode (cname = "clutter_actor_has_clip")]
		public bool get_has_clip ();
		public bool is_rotated ();
		public bool is_scaled ();
		public void lower (Clutter.Actor above);
		public void lower_bottom ();
		public void move_anchor_point (int anchor_x, int anchor_y);
		public void move_anchor_point_from_gravity (Clutter.Gravity gravity);
		public void move_anchor_pointu (Clutter.Unit anchor_x, Clutter.Unit anchor_y);
		public void move_by (int dx, int dy);
		public void move_byu (Clutter.Unit dx, Clutter.Unit dy);
		public void queue_redraw ();
		public void raise (Clutter.Actor below);
		public void raise_top ();
		public void remove_clip ();
		public void reparent (Clutter.Actor new_parent);
		public void set_anchor_point (int anchor_x, int anchor_y);
		public void set_anchor_point_from_gravity (Clutter.Gravity gravity);
		public void set_anchor_pointu (Clutter.Unit anchor_x, Clutter.Unit anchor_y);
		public void set_clip (int xoff, int yoff, int width, int height);
		public void set_clipu (Clutter.Unit xoff, Clutter.Unit yoff, Clutter.Unit width, Clutter.Unit height);
		public void set_depth (int depth);
		public void set_depthu (Clutter.Unit depth);
		public void set_geometry (Clutter.Geometry geometry);
		public void set_height (uint height);
		public void set_heightu (Clutter.Unit height);
		public void set_name (string name);
		public void set_opacity (uchar opacity);
		public void set_parent (Clutter.Actor parent);
		public void set_position (int x, int y);
		public void set_positionu (Clutter.Unit x, Clutter.Unit y);
		public void set_reactive (bool reactive);
		public void set_rotation (Clutter.RotateAxis axis, double angle, int x, int y, int z);
		public void set_rotationx (Clutter.RotateAxis axis, Clutter.Fixed angle, int x, int y, int z);
		public void set_scale (double scale_x, double scale_y);
		public void set_scalex (Clutter.Fixed scale_x, Clutter.Fixed scale_y);
		public bool set_shader (Clutter.Shader shader);
		public void set_shader_param (string param, float value);
		public void set_size (int width, int height);
		public void set_sizeu (Clutter.Unit width, Clutter.Unit height);
		public void set_width (uint width);
		public void set_widthu (Clutter.Unit width);
		public void set_x (int x);
		public void set_xu (Clutter.Unit x);
		public void set_y (int y);
		public void set_yu (Clutter.Unit y);
		public bool should_pick_paint ();
		public bool transform_stage_point (Clutter.Unit x, Clutter.Unit y, out Clutter.Unit x_out, out Clutter.Unit y_out);
		public void unparent ();
		public virtual void hide_all ();
		public virtual void paint ();
		public virtual void pick (Clutter.Color color);
		public virtual void query_coords (Clutter.ActorBox box);
		public virtual void realize ();
		public virtual void request_coords (Clutter.ActorBox box);
		public virtual void show_all ();
		public virtual void unrealize ();
		public weak Clutter.Geometry clip { get; set; }
		public weak int depth { get; set; }
		[NoAccessorMethod]
		public weak bool has_clip { get; }
		public weak int height { get; set; }
		public weak string name { get; set; }
		public weak uchar opacity { get; set; }
		public weak bool reactive { get; set; }
		[NoAccessorMethod]
		public weak double rotation_angle_x { get; set; }
		[NoAccessorMethod]
		public weak double rotation_angle_y { get; set; }
		[NoAccessorMethod]
		public weak double rotation_angle_z { get; set; }
		[NoAccessorMethod]
		public weak Clutter.Vertex rotation_center_x { get; set; }
		[NoAccessorMethod]
		public weak Clutter.Vertex rotation_center_y { get; set; }
		[NoAccessorMethod]
		public weak Clutter.Vertex rotation_center_z { get; set; }
		[NoAccessorMethod]
		public weak double scale_x { get; set; }
		[NoAccessorMethod]
		public weak double scale_y { get; set; }
		[NoAccessorMethod]
		public weak bool visible { get; set; }
		public weak int width { get; set; }
		public weak int x { get; set; }
		public weak int y { get; set; }
		public signal bool button_press_event (Clutter.Event event);
		public signal bool button_release_event (Clutter.Event event);
		public signal bool captured_event (Clutter.Event event);
		[HasEmitter]
		public signal void destroy ();
		public signal bool enter_event (Clutter.Event event);
		[HasEmitter]
		public signal bool event (Clutter.Event event);
		public signal void focus_in ();
		public signal void focus_out ();
		[HasEmitter]
		public signal void hide ();
		public signal bool key_press_event (Clutter.Event event);
		public signal bool key_release_event (Clutter.Event event);
		public signal bool leave_event (Clutter.Event event);
		public signal bool motion_event (Clutter.Event event);
		public signal void parent_set (Clutter.Actor old_parent);
		public signal bool scroll_event (Clutter.Event event);
		[HasEmitter]
		public signal void show ();
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class BehaviourPath : Clutter.Behaviour, Clutter.Scriptable {
		public BehaviourPath (Clutter.Alpha alpha, Clutter.Knot[] knots, uint n_knots);
		public void append_knot (ref Clutter.Knot knot);
		public void append_knots (...);
		public void clear ();
		public weak GLib.SList get_knots ();
		public void insert_knot (uint offset, Clutter.Knot knot);
		public void remove_knot (uint offset);
		[NoAccessorMethod]
		public weak Clutter.Knot knot { set; }
		public signal void knot_reached (Clutter.Knot knot);
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class BehaviourBspline : Clutter.Behaviour, Clutter.Scriptable {
		public BehaviourBspline (Clutter.Alpha alpha, Clutter.Knot[] knots, uint n_knots);
		public void adjust (uint offset, Clutter.Knot knot);
		public void append_knot (ref Clutter.Knot knot);
		public void append_knots (...);
		public void clear ();
		public void get_origin (out Clutter.Knot knot);
		public void join (Clutter.BehaviourBspline bs2);
		public void set_origin (Clutter.Knot knot);
		public weak Clutter.Behaviour split (uint offset);
		public void truncate (uint offset);
		public signal void knot_reached (Clutter.Knot knot);
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class COGLhandle {
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class Angle {
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class AnyEvent {
		public Clutter.EventType type;
		public uint time;
		public Clutter.EventFlags flags;
		public weak Clutter.Actor source;
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class ButtonEvent {
		public Clutter.EventType type;
		public uint time;
		public Clutter.EventFlags flags;
		public weak Clutter.Actor source;
		public int x;
		public int y;
		public Clutter.ModifierType modifier_state;
		public uint button;
		public uint click_count;
		public double axes;
		public weak Clutter.InputDevice device;
		[CCode (cname = "clutter_button_event_button")]
		public uint get_button ();
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class CrossingEvent {
		public Clutter.EventType type;
		public uint time;
		public Clutter.EventFlags flags;
		public weak Clutter.Actor source;
		public int x;
		public int y;
		public weak Clutter.Actor related;
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class InputDevice {
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class KeyEvent {
		public Clutter.EventType type;
		public uint time;
		public Clutter.EventFlags flags;
		public weak Clutter.Actor source;
		public Clutter.ModifierType modifier_state;
		public uint keyval;
		public ushort hardware_keycode;
		public unichar unicode_value;
		[CCode (cname = "clutter_key_event_code")]
		public ushort get_code ();
		[CCode (cname = "clutter_key_event_symbol")]
		public uint get_symbol ();
		[CCode (cname = "clutter_key_event_unicode")]
		public uint get_unicode ();
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class MotionEvent {
		public Clutter.EventType type;
		public uint time;
		public Clutter.EventFlags flags;
		public weak Clutter.Actor source;
		public int x;
		public int y;
		public Clutter.ModifierType modifier_state;
		public double axes;
		public weak Clutter.InputDevice device;
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class ScrollEvent {
		public Clutter.EventType type;
		public uint time;
		public Clutter.EventFlags flags;
		public weak Clutter.Actor source;
		public int x;
		public int y;
		public Clutter.ScrollDirection direction;
		public Clutter.ModifierType modifier_state;
		public double axes;
		public weak Clutter.InputDevice device;
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class StageStateEvent {
		public Clutter.EventType type;
		public uint time;
		public Clutter.EventFlags flags;
		public weak Clutter.Actor source;
		public Clutter.StageState changed_mask;
		public Clutter.StageState new_state;
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class TimeoutPool {
		public uint add (uint interval, GLib.SourceFunc func, pointer data, GLib.DestroyNotify notify);
		public TimeoutPool (int priority);
		public void remove (uint id);
	}
	[CCode (copy_function = "clutter_event_copy", cheader_filename = "clutter/clutter.h")]
	public class Event : GLib.Boxed {
		public Clutter.EventType type;
		public weak Clutter.AnyEvent any;
		public weak Clutter.ButtonEvent button;
		public weak Clutter.KeyEvent key;
		public weak Clutter.MotionEvent motion;
		public weak Clutter.ScrollEvent scroll;
		public weak Clutter.StageStateEvent stage_state;
		public weak Clutter.CrossingEvent crossing;
		public weak Clutter.Event copy ();
		public static weak Clutter.Event get ();
		public void get_coords (out int x, out int y);
		public weak Clutter.Actor get_source ();
		public Clutter.ModifierType get_state ();
		public uint get_time ();
		public Event (Clutter.EventType type);
		public static weak Clutter.Event peek ();
		public void put ();
		[CCode (cname = "clutter_event_type")]
		public Clutter.EventType get_type ();
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class Geometry : GLib.Boxed {
		public int x;
		public int y;
		public uint width;
		public uint height;
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class Alpha : GLib.Object {
		public uint get_alpha ();
		public weak Clutter.Timeline get_timeline ();
		public Alpha ();
		public Alpha.full (Clutter.Timeline timeline, Clutter.AlphaFunc func, pointer data, GLib.DestroyNotify destroy);
		public void set_func (Clutter.AlphaFunc func, pointer data, GLib.DestroyNotify destroy);
		public void set_timeline (Clutter.Timeline timeline);
		public weak uint alpha { get; }
		public weak Clutter.Timeline timeline { get; set; }
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class Backend : GLib.Object {
		public uint get_double_click_distance ();
		public uint get_double_click_time ();
		public double get_resolution ();
		public void set_double_click_distance (uint distance);
		public void set_double_click_time (uint msec);
		public void set_resolution (double dpi);
		[NoWrapper]
		public virtual void add_options (GLib.OptionGroup group);
		[NoWrapper]
		public virtual Clutter.FeatureFlags get_features ();
		[NoWrapper]
		public virtual weak Clutter.Actor get_stage ();
		[NoWrapper]
		public virtual void init_events ();
		[NoWrapper]
		public virtual void init_features ();
		[NoWrapper]
		public virtual bool init_stage () throws GLib.Error;
		[NoWrapper]
		public virtual bool post_parse () throws GLib.Error;
		[NoWrapper]
		public virtual bool pre_parse () throws GLib.Error;
		[NoWrapper]
		public virtual void redraw ();
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class Behaviour : GLib.Object {
		public void actors_foreach (Clutter.BehaviourForeachFunc func, pointer data);
		public void apply (Clutter.Actor actor);
		public weak GLib.SList get_actors ();
		public weak Clutter.Alpha get_alpha ();
		public int get_n_actors ();
		public weak Clutter.Actor get_nth_actor (int index_);
		public bool is_applied (Clutter.Actor actor);
		public void remove_all ();
		public void set_alpha (Clutter.Alpha alpha);
		[NoWrapper]
		public virtual void alpha_notify (uint alpha_value);
		public weak Clutter.Alpha alpha { get; set; }
		public signal void applied (Clutter.Actor actor);
		public signal void removed (Clutter.Actor actor);
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class BehaviourDepth : Clutter.Behaviour {
		public void get_bounds (out int depth_start, out int depth_end);
		public BehaviourDepth (Clutter.Alpha alpha, int depth_start, int depth_end);
		public void set_bounds (int depth_start, int depth_end);
		[NoAccessorMethod]
		public weak int depth_end { get; set; }
		[NoAccessorMethod]
		public weak int depth_start { get; set; }
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class BehaviourEllipse : Clutter.Behaviour {
		public double get_angle_end ();
		public Clutter.Fixed get_angle_endx ();
		public double get_angle_start ();
		public Clutter.Fixed get_angle_startx ();
		public double get_angle_tilt (Clutter.RotateAxis axis);
		public Clutter.Fixed get_angle_tiltx (Clutter.RotateAxis axis);
		public void get_center (out int x, out int y);
		public Clutter.RotateDirection get_direction ();
		public int get_height ();
		public void get_tilt (out double angle_tilt_x, out double angle_tilt_y, out double angle_tilt_z);
		public void get_tiltx (Clutter.Fixed angle_tilt_x, Clutter.Fixed angle_tilt_y, Clutter.Fixed angle_tilt_z);
		public int get_width ();
		public BehaviourEllipse (Clutter.Alpha alpha, int x, int y, int width, int height, Clutter.RotateDirection direction, double start, double end);
		[CCode (cname = "clutter_behaviour_ellipse_newx")]
		public BehaviourEllipse.newx (Clutter.Alpha alpha, int x, int y, int width, int height, Clutter.RotateDirection direction, Clutter.Fixed start, Clutter.Fixed end);
		public void set_angle_end (double angle_end);
		public void set_angle_endx (Clutter.Fixed angle_end);
		public void set_angle_start (double angle_start);
		public void set_angle_startx (Clutter.Fixed angle_start);
		public void set_angle_tilt (Clutter.RotateAxis axis, double angle_tilt);
		public void set_angle_tiltx (Clutter.RotateAxis axis, Clutter.Fixed angle_tilt);
		public void set_center (int x, int y);
		public void set_direction (Clutter.RotateDirection direction);
		public void set_height (int height);
		public void set_tilt (double angle_tilt_x, double angle_tilt_y, double angle_tilt_z);
		public void set_tiltx (Clutter.Fixed angle_tilt_x, Clutter.Fixed angle_tilt_y, Clutter.Fixed angle_tilt_z);
		public void set_width (int width);
		[NoWrapper]
		public virtual void knot_reached (Clutter.Knot knot);
		public weak double angle_end { get; set; }
		public weak double angle_start { get; set; }
		[NoAccessorMethod]
		public weak double angle_tilt_x { get; set; }
		[NoAccessorMethod]
		public weak double angle_tilt_y { get; set; }
		[NoAccessorMethod]
		public weak double angle_tilt_z { get; set; }
		public weak Clutter.Knot center { get; set; }
		public weak Clutter.RotateDirection direction { get; set; }
		public weak int height { get; set; }
		public weak int width { get; set; }
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class BehaviourOpacity : Clutter.Behaviour {
		public void get_bounds (out uchar opacity_start, out uchar opacity_end);
		public BehaviourOpacity (Clutter.Alpha alpha, uchar opacity_start, uchar opacity_end);
		public void set_bounds (uchar opacity_start, uchar opacity_end);
		[NoAccessorMethod]
		public weak uint opacity_end { get; set; }
		[NoAccessorMethod]
		public weak uint opacity_start { get; set; }
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class BehaviourRotate : Clutter.Behaviour {
		public Clutter.RotateAxis get_axis ();
		public void get_bounds (out double angle_start, out double angle_end);
		public void get_boundsx (Clutter.Fixed angle_start, Clutter.Fixed angle_end);
		public void get_center (int x, int y, int z);
		public Clutter.RotateDirection get_direction ();
		public BehaviourRotate (Clutter.Alpha alpha, Clutter.RotateAxis axis, Clutter.RotateDirection direction, double angle_start, double angle_end);
		[CCode (cname = "clutter_behaviour_rotate_newx")]
		public BehaviourRotate.newx (Clutter.Alpha alpha, Clutter.RotateAxis axis, Clutter.RotateDirection direction, Clutter.Fixed angle_start, Clutter.Fixed angle_end);
		public void set_axis (Clutter.RotateAxis axis);
		public void set_bounds (double angle_start, double angle_end);
		public void set_boundsx (Clutter.Fixed angle_start, Clutter.Fixed angle_end);
		public void set_center (int x, int y, int z);
		public void set_direction (Clutter.RotateDirection direction);
		[NoAccessorMethod]
		public weak double angle_end { get; set; }
		[NoAccessorMethod]
		public weak double angle_start { get; set; }
		public weak Clutter.RotateAxis axis { get; set; }
		[NoAccessorMethod]
		public weak int center_x { get; set; }
		[NoAccessorMethod]
		public weak int center_y { get; set; }
		[NoAccessorMethod]
		public weak int center_z { get; set; }
		public weak Clutter.RotateDirection direction { get; set; }
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class BehaviourScale : Clutter.Behaviour {
		public void get_bounds (out double x_scale_start, out double y_scale_start, out double x_scale_end, out double y_scale_end);
		public void get_boundsx (Clutter.Fixed x_scale_start, Clutter.Fixed y_scale_start, Clutter.Fixed x_scale_end, Clutter.Fixed y_scale_end);
		public BehaviourScale (Clutter.Alpha alpha, double x_scale_start, double y_scale_start, double x_scale_end, double y_scale_end);
		[CCode (cname = "clutter_behaviour_scale_newx")]
		public BehaviourScale.newx (Clutter.Alpha alpha, Clutter.Fixed x_scale_start, Clutter.Fixed y_scale_start, Clutter.Fixed x_scale_end, Clutter.Fixed y_scale_end);
		public void set_bounds (double x_scale_start, double y_scale_start, double x_scale_end, double y_scale_end);
		public void set_boundsx (Clutter.Fixed x_scale_start, Clutter.Fixed y_scale_start, Clutter.Fixed x_scale_end, Clutter.Fixed y_scale_end);
		[NoAccessorMethod]
		public weak double x_scale_end { get; set; }
		[NoAccessorMethod]
		public weak double x_scale_start { get; set; }
		[NoAccessorMethod]
		public weak double y_scale_end { get; set; }
		[NoAccessorMethod]
		public weak double y_scale_start { get; set; }
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class CloneTexture : Clutter.Actor, Clutter.Scriptable {
		public weak Clutter.Texture get_parent_texture ();
		public CloneTexture (Clutter.Texture texture);
		public void set_parent_texture (Clutter.Texture texture);
		public weak Clutter.Texture parent_texture { get; set; }
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class EffectTemplate : GLib.Object {
		public void @construct (Clutter.Timeline timeline, Clutter.AlphaFunc alpha_func, pointer user_data, GLib.DestroyNotify notify);
		public bool get_timeline_clone ();
		public EffectTemplate (Clutter.Timeline timeline, Clutter.AlphaFunc alpha_func);
		public EffectTemplate.for_duration (uint msecs, Clutter.AlphaFunc alpha_func);
		public EffectTemplate.full (Clutter.Timeline timeline, Clutter.AlphaFunc alpha_func, pointer user_data, GLib.DestroyNotify notify);
		public void set_timeline_clone (bool setting);
		[NoAccessorMethod]
		public weak bool clone { get; set; }
		[NoAccessorMethod]
		public weak Clutter.Timeline timeline { get; construct; }
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class Entry : Clutter.Actor, Clutter.Scriptable {
		public void delete_chars (uint len);
		public void delete_text (long start_pos, long end_pos);
		public Pango.Alignment get_alignment ();
		public void get_color (Clutter.Color color);
		public int get_cursor_position ();
		public weak string get_font_name ();
		public unichar get_invisible_char ();
		public weak Pango.Layout get_layout ();
		public int get_max_length ();
		public weak string get_text ();
		public bool get_visibility ();
		public bool get_visible_cursor ();
		public void handle_key_event (Clutter.KeyEvent kev);
		public void insert_text (string text, long position);
		public void insert_unichar (unichar wc);
		public Entry ();
		public Entry.full (string font_name, string text, Clutter.Color color);
		public Entry.with_text (string font_name, string text);
		public void set_alignment (Pango.Alignment alignment);
		public void set_color (Clutter.Color color);
		public void set_cursor_position (int position);
		public void set_font_name (string font_name);
		public void set_invisible_char (unichar wc);
		public void set_max_length (int max);
		public void set_text (string text);
		public void set_visibility (bool visible);
		public void set_visible_cursor (bool visible);
		[NoWrapper]
		public virtual void paint_cursor ();
		public weak Pango.Alignment alignment { get; set; }
		public weak Clutter.Color color { get; set; }
		[NoAccessorMethod]
		public weak bool cursor_visible { get; set; }
		[NoAccessorMethod]
		public weak uint entry_padding { get; set; }
		public weak string font_name { get; set; }
		public weak int max_length { get; set; }
		[NoAccessorMethod]
		public weak int position { get; set; }
		public weak string text { get; set; }
		[NoAccessorMethod]
		public weak bool text_visible { get; set; }
		[NoAccessorMethod]
		public weak double x_align { get; set; }
		public signal void activate ();
		public signal void cursor_event (Clutter.Geometry geometry);
		public signal void text_changed ();
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class Group : Clutter.Actor, Clutter.Scriptable, Clutter.Container {
		public int get_n_children ();
		public weak Clutter.Actor get_nth_child (int index_);
		public Group ();
		public void remove_all ();
		public signal void add (Clutter.Actor child);
		public signal void remove (Clutter.Actor child);
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class Label : Clutter.Actor, Clutter.Scriptable {
		public Pango.Alignment get_alignment ();
		public weak Pango.AttrList get_attributes ();
		public void get_color (out Clutter.Color color);
		public Pango.EllipsizeMode get_ellipsize ();
		public weak string get_font_name ();
		public bool get_justify ();
		public weak Pango.Layout get_layout ();
		public bool get_line_wrap ();
		public Pango.WrapMode get_line_wrap_mode ();
		public weak string get_text ();
		public bool get_use_markup ();
		public Label ();
		public Label.full (string font_name, string text, Clutter.Color color);
		public Label.with_text (string font_name, string text);
		public void set_alignment (Pango.Alignment alignment);
		public void set_attributes (Pango.AttrList attrs);
		public void set_color (ref Clutter.Color color);
		public void set_ellipsize (Pango.EllipsizeMode mode);
		public void set_font_name (string font_name);
		public void set_justify (bool justify);
		public void set_line_wrap (bool wrap);
		public void set_line_wrap_mode (Pango.WrapMode wrap_mode);
		public void set_text (string text);
		public void set_use_markup (bool setting);
		public weak Pango.Alignment alignment { get; set; }
		public weak Pango.AttrList attributes { get; set; }
		public weak Clutter.Color color { get; set; }
		public weak Pango.EllipsizeMode ellipsize { get; set; }
		public weak string font_name { get; set; }
		public weak bool justify { get; set; }
		public weak string text { get; set; }
		public weak bool use_markup { get; set; }
		[NoAccessorMethod]
		public weak bool wrap { get; set; }
		[NoAccessorMethod]
		public weak Pango.WrapMode wrap_mode { get; set; }
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class ListModel : Clutter.Model {
		public ListModel (uint n_columns);
		[CCode (cname = "clutter_list_model_newv")]
		public ListModel.newv (uint n_columns, GLib.Type[] types, string[] names);
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class Model : GLib.Object {
		public void append ();
		public void appendv (uint n_columns, uint columns, GLib.Value values);
		public bool filter_iter (Clutter.ModelIter iter);
		public bool filter_row (uint row);
		public void @foreach (Clutter.ModelForeachFunc func, pointer user_data);
		public weak Clutter.ModelIter get_first_iter ();
		public weak Clutter.ModelIter get_last_iter ();
		public int get_sorting_column ();
		public void insert (uint row);
		public void insert_value (uint row, uint column, GLib.Value value);
		public void insertv (uint row, uint n_columns, uint columns, GLib.Value values);
		public void prepend ();
		public void prependv (uint n_columns, uint columns, GLib.Value values);
		public void remove (uint row);
		public void set_filter (Clutter.ModelFilterFunc func, pointer user_data, GLib.DestroyNotify notify);
		public void set_names (uint n_columns, string[] names);
		public void set_sort (uint column, Clutter.ModelSortFunc func, pointer user_data, GLib.DestroyNotify notify);
		public void set_sorting_column (int column);
		public void set_types (uint n_columns, GLib.Type[] types);
		public virtual weak string get_column_name (uint column);
		public virtual GLib.Type get_column_type (uint column);
		public virtual weak Clutter.ModelIter get_iter_at_row (uint row);
		public virtual uint get_n_columns ();
		public virtual uint get_n_rows ();
		[NoWrapper]
		public virtual weak Clutter.ModelIter insert_row (int index_);
		[NoWrapper]
		public virtual void remove_row (uint row);
		public virtual void resort ();
		public signal void filter_changed ();
		public signal void row_added (Clutter.ModelIter iter);
		public signal void row_changed (Clutter.ModelIter iter);
		public signal void row_removed (Clutter.ModelIter iter);
		public signal void sort_changed ();
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class ModelIter : GLib.Object {
		public void get ();
		public void get_valist (pointer args);
		public void set ();
		public void set_valist (pointer args);
		public virtual weak Clutter.Model get_model ();
		public virtual uint get_row ();
		public virtual void get_value (uint column, GLib.Value value);
		public virtual bool is_first ();
		public virtual bool is_last ();
		public virtual weak Clutter.ModelIter next ();
		public virtual weak Clutter.ModelIter prev ();
		public virtual void set_value (uint column, GLib.Value value);
		[NoAccessorMethod]
		public weak Clutter.Model model { get; set; }
		[NoAccessorMethod]
		public weak uint row { get; set; }
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class Rectangle : Clutter.Actor, Clutter.Scriptable {
		public void get_border_color (out Clutter.Color color);
		public uint get_border_width ();
		public void get_color (out Clutter.Color color);
		public Rectangle ();
		public Rectangle.with_color (Clutter.Color color);
		public void set_border_color (ref Clutter.Color color);
		public void set_border_width (uint width);
		public void set_color (ref Clutter.Color color);
		public weak Clutter.Color border_color { get; set; }
		public weak uint border_width { get; set; }
		public weak Clutter.Color color { get; set; }
		[NoAccessorMethod]
		public weak bool has_border { get; set; }
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class Score : GLib.Object {
		public uint append (Clutter.Timeline parent, Clutter.Timeline timeline);
		public bool get_loop ();
		public weak Clutter.Timeline get_timeline (uint id);
		public bool is_playing ();
		public weak GLib.SList list_timelines ();
		public Score ();
		public void pause ();
		public void remove (uint id);
		public void remove_all ();
		public void rewind ();
		public void set_loop (bool loop);
		public void start ();
		public void stop ();
		public weak bool loop { get; set; }
		public signal void completed ();
		public signal void paused ();
		public signal void started ();
		public signal void timeline_completed (Clutter.Timeline timeline);
		public signal void timeline_started (Clutter.Timeline timeline);
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class Script : GLib.Object {
		public void connect_signals (pointer user_data);
		public void connect_signals_full (Clutter.ScriptConnectFunc func, pointer user_data);
		public void ensure_objects ();
		public static GLib.Quark error_quark ();
		public weak GLib.Object get_object (string name);
		public int get_objects (...);
		public uint load_from_data (string data, long length) throws GLib.Error;
		public uint load_from_file (string filename) throws GLib.Error;
		public Script ();
		public void unmerge_objects (uint merge_id);
		public virtual GLib.Type get_type_from_name (string type_name);
		[NoAccessorMethod]
		public weak string filename { get; }
		[NoAccessorMethod]
		public weak bool filename_set { get; }
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class Shader : GLib.Object {
		public bool bind () throws GLib.Error;
		public static GLib.Quark error_quark ();
		public weak string get_fragment_source ();
		public bool get_is_enabled ();
		public weak string get_vertex_source ();
		public bool is_bound ();
		public Shader ();
		public void release ();
		public static void release_all ();
		public void set_fragment_source (string data, long length);
		public void set_is_enabled (bool enabled);
		public void set_uniform_1f (string name, float value);
		public void set_vertex_source (string data, long length);
		[NoAccessorMethod]
		public weak bool bound { get; }
		[NoAccessorMethod]
		public weak bool enabled { get; set; }
		public weak string fragment_source { get; set; }
		public weak string vertex_source { get; set; }
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class Stage : Clutter.Group, Clutter.Scriptable, Clutter.Container {
		public bool event (Clutter.Event event);
		public weak Clutter.Actor get_actor_at_pos (int x, int y);
		public void get_color (out Clutter.Color color);
		public static weak Clutter.Actor get_default ();
		public void get_fog (out double density, out double z_near, out double z_far);
		public void get_fogx (out Clutter.Fog fog);
		public weak Clutter.Actor get_key_focus ();
		public void get_perspective (out float fovy, out float aspect, out float z_near, out float z_far);
		public void get_perspectivex (out Clutter.Perspective perspective);
		public double get_resolution ();
		public Clutter.Fixed get_resolutionx ();
		public weak string get_title ();
		public bool get_use_fog ();
		public bool get_user_resizable ();
		public void hide_cursor ();
		public void set_color (ref Clutter.Color color);
		public void set_fog (double density, double z_near, double z_far);
		public void set_fogx (ref Clutter.Fog fog);
		public void set_key_focus (Clutter.Actor actor);
		public void set_perspective (float fovy, float aspect, float z_near, float z_far);
		public void set_perspectivex (ref Clutter.Perspective perspective);
		public void set_use_fog (bool fog);
		public void set_user_resizable (bool resizable);
		public void show_cursor ();
		// public weak Gdk.Pixbuf snapshot (int x, int y, int width, int height);
		// [NoWrapper]
		// public virtual weak Gdk.Pixbuf draw_to_pixbuf (int x, int y, int width, int height);
		[NoWrapper]
		public virtual void set_cursor_visible (bool visible);
		[NoWrapper]
		public virtual void set_fullscreen (bool fullscreen);
		public virtual void set_title (string title);
		[NoWrapper]
		public virtual void set_user_resize (bool value);
		public weak Clutter.Color color { get; set; }
		[NoAccessorMethod]
		public weak bool cursor_visible { get; set construct; }
		[NoAccessorMethod]
		public weak bool offscreen { get; set construct; }
		public weak string title { get; set; }
		public weak bool use_fog { get; set; }
		public weak bool user_resizable { get; set construct; }
		public signal void activate ();
		public signal void deactivate ();
		[HasEmitter]
		public signal void fullscreen ();
		[HasEmitter]
		public signal void unfullscreen ();
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class Texture : Clutter.Actor, Clutter.Scriptable {
		public void bind_tile (int index_);
		public static GLib.Quark error_quark ();
		public void get_base_size (out int width, out int height);
		public void get_n_tiles (out int n_x_tiles, out int n_y_tiles);
		// public weak Gdk.Pixbuf get_pixbuf ();
		public void get_x_tile_detail (int x_index, out int pos, out int size, out int waste);
		public void get_y_tile_detail (int y_index, out int pos, out int size, out int waste);
		public bool has_generated_tiles ();
		public bool is_tiled ();
		public Texture ();
		public Texture.from_actor (Clutter.Actor actor);
		public Texture.from_pixbuf (Gdk.Pixbuf pixbuf);
		public bool set_area_from_rgb_data (uchar[] data, bool has_alpha, int x, int y, int width, int height, int rowstride, int bpp, Clutter.TextureFlags flags) throws GLib.Error;
		public bool set_from_rgb_data (uchar[] data, bool has_alpha, int width, int height, int rowstride, int bpp, Clutter.TextureFlags flags) throws GLib.Error;
		public bool set_from_yuv_data (uchar[] data, int width, int height, Clutter.TextureFlags flags) throws GLib.Error;
		// public bool set_pixbuf (Gdk.Pixbuf pixbuf) throws GLib.Error;
		[NoAccessorMethod]
		public weak int filter_quality { get; set; }
		// public weak Gdk.Pixbuf pixbuf { get; set; }
		[NoAccessorMethod]
		public weak int pixel_format { get; }
		[NoAccessorMethod]
		public weak int pixel_type { get; }
		[NoAccessorMethod]
		public weak bool repeat_x { get; set; }
		[NoAccessorMethod]
		public weak bool repeat_y { get; set; }
		[NoAccessorMethod]
		public weak bool sync_size { get; set; }
		[NoAccessorMethod]
		public weak int tile_waste { get; construct; }
		[NoAccessorMethod]
		public weak bool tiled { get; construct; }
		public signal void pixbuf_change ();
		public signal void size_change (int width, int height);
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public class Timeline : GLib.Object {
		public void advance (uint frame_num);
		public weak Clutter.Timeline clone ();
		public int get_current_frame ();
		public uint get_delay ();
		public uint get_delta (out uint msecs);
		public Clutter.TimelineDirection get_direction ();
		public uint get_duration ();
		public bool get_loop ();
		public uint get_n_frames ();
		public double get_progress ();
		public Clutter.Fixed get_progressx ();
		public uint get_speed ();
		public bool is_playing ();
		public Timeline (uint n_frames, uint fps);
		public Timeline.for_duration (uint msecs);
		public void pause ();
		public void rewind ();
		public void set_delay (uint msecs);
		public void set_direction (Clutter.TimelineDirection direction);
		public void set_duration (uint msecs);
		public void set_loop (bool loop);
		public void set_n_frames (uint n_frames);
		public void set_speed (uint fps);
		public void skip (uint n_frames);
		public void start ();
		public void stop ();
		public weak uint delay { get; set; }
		public weak Clutter.TimelineDirection direction { get; set; }
		public weak uint duration { get; set; }
		[NoAccessorMethod]
		public weak uint fps { get; set; }
		public weak bool loop { get; set; }
		[NoAccessorMethod]
		public weak uint num_frames { get; set; }
		public signal void completed ();
		public signal void new_frame (int frame_num);
		public signal void paused ();
		public signal void started ();
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public interface Scriptable {
		public abstract weak string get_id ();
		public abstract bool parse_custom_node (Clutter.Script script, GLib.Value value, string name, pointer node);
		public abstract void set_custom_property (Clutter.Script script, string name, GLib.Value value);
		public abstract void set_id (string id);
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public interface Container : Clutter.Actor {
		public void add_actor (Clutter.Actor actor);
		public void add_valist (Clutter.Actor first_actor, pointer var_args);
		public weak Clutter.Actor find_child_by_name (string child_name);
		public weak GLib.List get_children ();
		public void lower_child (Clutter.Actor actor, Clutter.Actor sibling);
		public void raise_child (Clutter.Actor actor, Clutter.Actor sibling);
		public void remove_actor (Clutter.Actor actor);
		public void remove_valist (Clutter.Actor first_actor, pointer var_args);
		public abstract void add (...);
		public abstract void @foreach (Clutter.Callback callback, pointer user_data);
		[NoWrapper]
		public abstract void lower (Clutter.Actor actor, Clutter.Actor sibling);
		[NoWrapper]
		public abstract void raise (Clutter.Actor actor, Clutter.Actor sibling);
		public abstract void remove (...);
		public abstract void sort_depth_order ();
		public signal void actor_added (Clutter.Actor actor);
		public signal void actor_removed (Clutter.Actor actor);
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public interface Media {
		public bool get_can_seek ();
		public void set_filename (string filename);
		public abstract int get_buffer_percent ();
		public abstract int get_duration ();
		public abstract bool get_playing ();
		public abstract int get_position ();
		public abstract weak string get_uri ();
		public abstract double get_volume ();
		public abstract void set_playing (bool playing);
		public abstract void set_position (int position);
		public abstract void set_uri (string uri);
		public abstract void set_volume (double volume);
		public signal void eos ();
		public signal void error (pointer error);
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	[SimpleType]
	public struct Fixed {
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	[SimpleType]
	public struct Unit {
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public struct ActorBox {
		public Clutter.Unit x1;
		public Clutter.Unit y1;
		public Clutter.Unit x2;
		public Clutter.Unit y2;
		public static void get_from_vertices (Clutter.Vertex[] vtx, Clutter.ActorBox box);
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public struct Color {
		public uchar red;
		public uchar green;
		public uchar blue;
		public uchar alpha;
		public void add (Clutter.Color src2, out Clutter.Color dest);
		public Clutter.Color copy ();
		public void darken (out Clutter.Color dest);
		public bool equal (Clutter.Color b);
		public void free ();
		public void from_hls (uchar hue, uchar luminance, uchar saturation);
		public void from_hlsx (Clutter.Fixed hue, Clutter.Fixed luminance, Clutter.Fixed saturation);
		public void from_pixel (uint pixel);
		public void lighten (out Clutter.Color dest);
		public static bool parse (string color, out Clutter.Color dest);
		public void shade (out Clutter.Color dest, double shade);
		public void shadex (Clutter.Color dest, Clutter.Fixed shade);
		public void subtract (Clutter.Color src2, out Clutter.Color dest);
		public void to_hls (out uchar hue, out uchar luminance, out uchar saturation);
		public void to_hlsx (Clutter.Fixed hue, Clutter.Fixed luminance, Clutter.Fixed saturation);
		public uint to_pixel ();
		public weak string to_string ();
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public struct Fog {
		public Clutter.Fixed density;
		public Clutter.Fixed z_near;
		public Clutter.Fixed z_far;
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public struct Knot {
		public int x;
		public int y;
		public Clutter.Knot copy ();
		public bool equal (Clutter.Knot knot_b);
		public void free ();
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public struct Perspective {
		public Clutter.Fixed fovy;
		public Clutter.Fixed aspect;
		public Clutter.Fixed z_near;
		public Clutter.Fixed z_far;
	}
	[CCode (cheader_filename = "clutter/clutter.h")]
	public struct Vertex {
		public Clutter.Unit x;
		public Clutter.Unit y;
		public Clutter.Unit z;
	}
	public static delegate uint AlphaFunc (Clutter.Alpha alpha, pointer user_data);
	public static delegate void BehaviourForeachFunc (Clutter.Behaviour behaviour, Clutter.Actor actor, pointer data);
	public static delegate void Callback (Clutter.Actor actor, pointer data);
	public static delegate void EffectCompleteFunc (Clutter.Actor actor, pointer user_data);
	public static delegate bool ModelFilterFunc (Clutter.Model model, Clutter.ModelIter iter, pointer user_data);
	public static delegate bool ModelForeachFunc (Clutter.Model model, Clutter.ModelIter iter, pointer user_data);
	public static delegate int ModelSortFunc (Clutter.Model model, GLib.Value a, GLib.Value b, pointer user_data);
	public static delegate void ScriptConnectFunc (Clutter.Script script, GLib.Object object, string signal_name, string handler_name, GLib.Object connect_object, GLib.ConnectFlags flags, pointer user_data);
	public static delegate void CoglFuncPtr ();
	public const int CFX_2PI;
	public const int CFX_HALF;
	public const int CFX_MAX;
	public const int CFX_MIN;
	public const int CFX_ONE;
	public const int CFX_PI;
	public const int CFX_PI_2;
	public const int CFX_PI_4;
	public const int CFX_Q;
    [CCode(cname="CGL_ENABLE_ALPHA_TEST")]
	public const int CGL_ENABLE_ALPHA_TEST;
    [CCode(cname="CGL_ENABLE_BLEND")]
	public const int CGL_ENABLE_BLEND;
    [CCode(cname="CGL_ENABLE_TEXTURE_2D")]
	public const int CGL_ENABLE_TEXTURE_2D;
    [CCode(cname="CGL_ENABLE_TEXTURE_RECT")]
	public const int CGL_ENABLE_TEXTURE_RECT;
    [CCode(cname="CGL_TEXTURE_RECTANGLE_ARB")]
	public const int CGL_TEXTURE_RECTANGLE_ARB;
    [CCode(cname="CGL_TEXTURE_2D")]
	public const int CGL_TEXTURE_2D;
    [CCode(cname="CGL_LINEAR")]
	public const int CGL_LINEAR;
    [CCode(cname="CGL_UNSIGNED_SHORT_8_8_MESA")]
	public const int CGL_UNSIGNED_SHORT_8_8_MESA;
    [CCode(cname="CGL_UNSIGNED_SHORT_8_8_REV_MESA")]
	public const int CGL_UNSIGNED_SHORT_8_8_REV_MESA;
    [CCode(cname="CGL_UNSIGNED_INT_8_8_8_8_REV")]
	public const int CGL_UNSIGNED_INT_8_8_8_8_REV;
    [CCode(cname="CGL_UNSIGNED_BYTE")]
	public const int CGL_UNSIGNED_BYTE;
    [CCode(cname="CGL_YCBCR_MESA")]
	public const int CGL_YCBCR_MESA;
    [CCode(cname="CGL_BGRA")]
    public const int CGL_BGRA;
    [CCode(cname="CGL_RGBA")]
    public const int CGL_RGBA;
	public const int ALPHA_MAX_ALPHA;
	public const string COGL;
	public const int COGL_HAS_GL;
	public const int CURRENT_TIME;
	public const string FLAVOUR;
	public const int MAJOR_VERSION;
	public const int MICRO_VERSION;
	public const int MINOR_VERSION;
	public const int SQRTI_ARG_10_PERCENT;
	public const int SQRTI_ARG_5_PERCENT;
	public const int SQRTI_ARG_MAX;
	public const int VERSION_HEX;
	public const string VERSION_S;
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static Clutter.InitError init ([CCode (array_length_pos = 0.9)] ref string[] args);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static void base_init ();
	[CCode (cname = "cogl_alpha_func", cheader_filename = "clutter/cogl.h")]
	public static void cogl_alpha_func (int func, Clutter.Fixed @ref);
	[CCode (cname = "cogl_check_extension", cheader_filename = "clutter/cogl.h")]
	public static bool cogl_check_extension (string name, string ext);
	[CCode (cname = "cogl_clip_set", cheader_filename = "clutter/cogl.h")]
	public static void cogl_clip_set (Clutter.Fixed x_offset, Clutter.Fixed y_offset, Clutter.Fixed width, Clutter.Fixed height);
	[CCode (cname = "cogl_clip_unset", cheader_filename = "clutter/cogl.h")]
	public static void cogl_clip_unset ();
	[CCode (cname = "cogl_color", cheader_filename = "clutter/cogl.h")]
	public static void cogl_color (Clutter.Color color);
	[CCode (cname = "cogl_create_program", cheader_filename = "clutter/cogl.h")]
	public static weak Clutter.COGLhandle cogl_create_program ();
	[CCode (cname = "cogl_create_shader", cheader_filename = "clutter/cogl.h")]
	public static weak Clutter.COGLhandle cogl_create_shader (int shaderType);
	[CCode (cname = "cogl_enable", cheader_filename = "clutter/cogl.h")]
	public static void cogl_enable (ulong flags);
	[CCode (cname = "cogl_enable_depth_test", cheader_filename = "clutter/cogl.h")]
	public static void cogl_enable_depth_test (bool setting);
	[CCode (cname = "cogl_fog_set", cheader_filename = "clutter/cogl.h")]
	public static void cogl_fog_set (Clutter.Color fog_color, Clutter.Fixed density, Clutter.Fixed z_near, Clutter.Fixed z_far);
	[CCode (cname = "cogl_get_bitmasks", cheader_filename = "clutter/cogl.h")]
	public static void cogl_get_bitmasks (int red, int green, int blue, int alpha);
	[CCode (cname = "cogl_get_features", cheader_filename = "clutter/cogl.h")]
	public static Clutter.FeatureFlags cogl_get_features ();
	[CCode (cname = "cogl_get_modelview_matrix", cheader_filename =
            "clutter/cogl.h")]
	public static void cogl_get_modelview_matrix (Clutter.Fixed[] m);
	[CCode (cname = "cogl_get_proc_address", cheader_filename = "clutter/cogl.h")]
	public static Clutter.CoglFuncPtr cogl_get_proc_address (string name);
	[CCode (cname = "cogl_get_projection_matrix", cheader_filename =
            "clutter/cogl.h")]
	public static void cogl_get_projection_matrix (Clutter.Fixed[] m);
	[CCode (cname = "cogl_get_viewport", cheader_filename = "clutter/cogl.h")]
	public static void cogl_get_viewport (Clutter.Fixed[] v);
	[CCode (cname = "cogl_offscreen_create", cheader_filename = "clutter/cogl.h")]
	public static weak uint cogl_offscreen_create (uint target_texture);
	[CCode (cname = "cogl_offscreen_destroy", cheader_filename = "clutter/cogl.h")]
	public static void cogl_offscreen_destroy (uint offscreen_handle);
	[CCode (cname = "cogl_offscreen_redirect_end", cheader_filename =
            "clutter/cogl.h")]
	public static void cogl_offscreen_redirect_end (uint offscreen_handle, int width, int height);
	[CCode (cname = "cogl_offscreen_redirect_start", cheader_filename =
            "clutter/cogl.h")]
	public static void cogl_offscreen_redirect_start (uint offscreen_handle, int width, int height);
	[CCode (cname = "cogl_paint_init", cheader_filename = "clutter/cogl.h")]
	public static void cogl_paint_init (Clutter.Color color);
	[CCode (cname = "cogl_perspective", cheader_filename = "clutter/cogl.h")]
	public static void cogl_perspective (Clutter.Fixed fovy, Clutter.Fixed aspect, Clutter.Fixed zNear, Clutter.Fixed zFar);
	[CCode (cname = "cogl_pop_matrix", cheader_filename = "clutter/cogl.h")]
	public static void cogl_pop_matrix ();
	[CCode (cname = "cogl_program_attach_shader", cheader_filename =
            "clutter/cogl.h")]
	public static void cogl_program_attach_shader (Clutter.COGLhandle program_handle, Clutter.COGLhandle shader_handle);
	[CCode (cname = "cogl_program_destroy", cheader_filename = "clutter/cogl.h")]
	public static void cogl_program_destroy (Clutter.COGLhandle handle);
	[CCode (cname = "cogl_program_get_uniform_location", cheader_filename =
            "clutter/cogl.h")]
	public static weak int cogl_program_get_uniform_location (Clutter.COGLhandle program_int, string uniform_name);
	[CCode (cname = "cogl_program_link", cheader_filename = "clutter/cogl.h")]
	public static void cogl_program_link (Clutter.COGLhandle program_handle);
	[CCode (cname = "cogl_program_uniform_1f", cheader_filename =
            "clutter/cogl.h")]
	public static void cogl_program_uniform_1f (int uniform_no, float value);
	[CCode (cname = "cogl_program_use", cheader_filename = "clutter/cogl.h")]
	public static void cogl_program_use (Clutter.COGLhandle program_handle);
	[CCode (cname = "cogl_push_matrix", cheader_filename = "clutter/cogl.h")]
	public static void cogl_push_matrix ();
	[CCode (cname = "cogl_rectangle", cheader_filename = "clutter/cogl.h")]
	public static void cogl_rectangle (int x, int y, uint width, uint height);
	[CCode (cname = "cogl_rotate", cheader_filename = "clutter/cogl.h")]
	public static void cogl_rotate (int angle, int x, int y, int z);
	[CCode (cname = "cogl_rotatex", cheader_filename = "clutter/cogl.h")]
	public static void cogl_rotatex (Clutter.Fixed angle, int x, int y, int z);
	[CCode (cname = "cogl_scale", cheader_filename = "clutter/cogl.h")]
	public static void cogl_scale (Clutter.Fixed x, Clutter.Fixed z);
	[CCode (cname = "cogl_setup_viewport", cheader_filename = "clutter/cogl.h")]
	public static void cogl_setup_viewport (uint width, uint height, Clutter.Fixed fovy, Clutter.Fixed aspect, Clutter.Fixed z_near, Clutter.Fixed z_far);
	[CCode (cname = "cogl_shader_compile", cheader_filename = "clutter/cogl.h")]
	public static void cogl_shader_compile (Clutter.COGLhandle shader_handle);
	[CCode (cname = "cogl_shader_destroy", cheader_filename = "clutter/cogl.h")]
	public static void cogl_shader_destroy (Clutter.COGLhandle handle);
	[CCode (cname = "cogl_shader_get_info_log", cheader_filename = "clutter/cogl.h")]
	public static void cogl_shader_get_info_log (Clutter.COGLhandle handle, uint size, string buffer);
	[CCode (cname = "cogl_shader_get_parameteriv", cheader_filename = "clutter/cogl.h")]
	public static void cogl_shader_get_parameteriv (Clutter.COGLhandle handle, int pname, int dest);
	[CCode (cname = "cogl_shader_source", cheader_filename = "clutter/cogl.h")]
	public static void cogl_shader_source (Clutter.COGLhandle shader, string source);
	[CCode (cname = "cogl_texture_bind", cheader_filename = "clutter/cogl.h")]
	public static void cogl_texture_bind (int target, uint texture);
	[CCode (cname = "cogl_texture_can_size", cheader_filename = "clutter/cogl.h")]
	public static bool cogl_texture_can_size (int target, int pixel_format, int pixel_type, int width, int height);
	[CCode (cname = "cogl_texture_image_2d", cheader_filename = "clutter/cogl.h")]
	public static void cogl_texture_image_2d (int target, int
            internal_format, int width, int height, int format, int type, uchar
            *pixels);
	[CCode (cname = "cogl_texture_quad", cheader_filename = "clutter/cogl.h")]
	public static void cogl_texture_quad (int x1, int x2, int y1, int y2, Clutter.Fixed tx1, Clutter.Fixed ty1, Clutter.Fixed tx2, Clutter.Fixed ty2);
	[CCode (cname = "cogl_texture_set_alignment", cheader_filename = "clutter/cogl.h")]
	public static void cogl_texture_set_alignment (int target, uint alignment, uint row_length);
	[CCode (cname = "cogl_texture_set_filters", cheader_filename = "clutter/cogl.h")]
	public static void cogl_texture_set_filters (int target, int min_filter, int max_filter);
	[CCode (cname = "cogl_texture_set_wrap", cheader_filename = "clutter/cogl.h")]
	public static void cogl_texture_set_wrap (int target, int wrap_s, int wrap_t);
	[CCode (cname = "cogl_texture_sub_image_2d", cheader_filename = "clutter/cogl.h")]
	public static void cogl_texture_sub_image_2d (int target, int xoff, int
            yoff, int width, int height, int format, int type, uchar *pixels);
	[CCode (cname = "cogl_textures_create", cheader_filename = "clutter/cogl.h")]
	public static void cogl_textures_create (uint num, uint textures);
	[CCode (cname = "cogl_textures_destroy", cheader_filename = "clutter/cogl.h")]
	public static void cogl_textures_destroy (uint num, uint textures);
	[CCode (cname = "cogl_translate", cheader_filename = "clutter/cogl.h")]
	public static void cogl_translate (int x, int y, int z);
	[CCode (cname = "cogl_translatex", cheader_filename = "clutter/cogl.h")]
	public static void cogl_translatex (Clutter.Fixed x, Clutter.Fixed y, Clutter.Fixed z);
	[CCode (cname = "cogl_trapezoid", cheader_filename = "clutter/cogl.h")]
	public static void cogl_trapezoid (int y1, int x11, int x21, int y2, int x12, int x22);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static void do_event (Clutter.Event event);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static weak Clutter.Timeline effect_depth (Clutter.EffectTemplate template_, Clutter.Actor actor, int depth_end, Clutter.EffectCompleteFunc func, pointer data);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static weak Clutter.Timeline effect_fade (Clutter.EffectTemplate template_, Clutter.Actor actor, uchar opacity_end, Clutter.EffectCompleteFunc func, pointer data);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static weak Clutter.Timeline effect_move (Clutter.EffectTemplate template_, Clutter.Actor actor, int x, int y, Clutter.EffectCompleteFunc func, pointer data);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static weak Clutter.Timeline effect_path (Clutter.EffectTemplate template_, Clutter.Actor actor, Clutter.Knot[] knots, Clutter.EffectCompleteFunc func, pointer data);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static weak Clutter.Timeline effect_rotate (Clutter.EffectTemplate template_, Clutter.Actor actor, Clutter.RotateAxis axis, double angle, int center_x, int center_y, int center_z, Clutter.RotateDirection direction, Clutter.EffectCompleteFunc func, pointer data);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static weak Clutter.Timeline effect_scale (Clutter.EffectTemplate template_, Clutter.Actor actor, double x_scale_end, double y_scale_end, Clutter.EffectCompleteFunc func, pointer data);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static bool events_pending ();
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static uint exp_dec_func (Clutter.Alpha alpha, pointer dummy);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static uint exp_inc_func (Clutter.Alpha alpha, pointer dummy);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static bool feature_available (Clutter.FeatureFlags feature);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static Clutter.FeatureFlags feature_get_all ();
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static weak Clutter.Actor get_actor_by_gid (uint id);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static bool get_debug_enabled ();
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static weak Clutter.Backend get_default_backend ();
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static uint get_default_frame_rate ();
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static weak Clutter.Actor get_keyboard_grab ();
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static bool get_motion_events_enabled ();
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static uint get_motion_events_frequency ();
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static weak GLib.OptionGroup get_option_group ();
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static weak Clutter.Actor get_pointer_grab ();
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static weak string get_script_id (GLib.Object gobject);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static bool get_show_fps ();
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static ulong get_timestamp ();
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static void grab_keyboard (Clutter.Actor actor);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static void grab_pointer (Clutter.Actor actor);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static GLib.Quark init_error_quark ();
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static Clutter.InitError init_with_args (int argc, string[] argv, string parameter_string, GLib.OptionEntry entries, string translation_domain) throws GLib.Error;
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static uint keysym_to_unicode (uint keyval);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static Clutter.Fixed log2x (uint x);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static void main ();
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static int main_level ();
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static void main_quit ();
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static uint pow2x (Clutter.Fixed x);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static uint powx (uint x, Clutter.Fixed y);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static Clutter.Fixed qdivx (Clutter.Fixed op1, Clutter.Fixed op2);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static Clutter.Fixed qmulx (Clutter.Fixed op1, Clutter.Fixed op2);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static uint ramp_dec_func (Clutter.Alpha alpha, pointer dummy);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static uint ramp_func (Clutter.Alpha alpha, pointer dummy);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static uint ramp_inc_func (Clutter.Alpha alpha, pointer dummy);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static void redraw ();
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static void set_default_frame_rate (uint frames_per_sec);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static void set_motion_events_enabled (bool enable);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static void set_motion_events_frequency (uint frequency);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static uint sine_dec_func (Clutter.Alpha alpha, pointer dummy);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static uint sine_func (Clutter.Alpha alpha, pointer dummy);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static uint sine_half_func (Clutter.Alpha alpha, pointer dummy);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static uint sine_inc_func (Clutter.Alpha alpha, pointer dummy);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static Clutter.Fixed sini (Clutter.Angle angle);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static Clutter.Fixed sinx (Clutter.Fixed angle);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static uint smoothstep_dec_func (Clutter.Alpha alpha, pointer dummy);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static uint smoothstep_inc_func (Clutter.Alpha alpha, pointer dummy);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static int sqrti (int x);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static Clutter.Fixed sqrtx (Clutter.Fixed x);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static uint square_func (Clutter.Alpha alpha, pointer dummy);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static Clutter.Fixed tani (Clutter.Angle angle);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static uint threads_add_idle (GLib.SourceFunc func, pointer data);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static uint threads_add_idle_full (int priority, GLib.SourceFunc func, pointer data, GLib.DestroyNotify notify);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static uint threads_add_timeout (uint interval, GLib.SourceFunc func, pointer data);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static uint threads_add_timeout_full (int priority, uint interval, GLib.SourceFunc func, pointer data, GLib.DestroyNotify notify);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static void threads_enter ();
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static void threads_init ();
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static void threads_leave ();
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static void threads_set_lock_functions (GLib.Callback enter_fn, GLib.Callback leave_fn);
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static void ungrab_keyboard ();
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static void ungrab_pointer ();
	[CCode (cheader_filename = "clutter/clutter.h")]
	public static int util_next_p2 (int a);
}
