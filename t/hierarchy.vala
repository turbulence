/*
 *  turbulence/t/hierarchy.vala
 *
 *  Copyright (c) 2008 Patrick Walton <pcwalton@uchicago.edu>
 */

using GLib;
using Gee;
using Clutter;
using Turbulence;

namespace TurbulenceTests {
    public class Hierarchy : Object {
        private bool to_add = true;

        public void frame_callback(Timeline tl, int frame_no)
        {
            if (frame_no != 0)
                return;

            if (to_add)
                grp_m.add_submodel(rect_m);
            else
                rect_m.remove_from_supermodel();

            to_add = !to_add;
        }

        ContainerModel grp_m;
        ActorModel rect_m; 

        construct {
            Clutter.Color blue, white, black;
            Clutter.Color.parse("blue", out blue);
            Clutter.Color.parse("white", out white);
            Clutter.Color.parse("black", out black);

            Stage stage = (Stage)Stage.get_default();
            stage.color = blue;
            stage.show();

            Clutter.Group grp_a = (Clutter.Group)new Clutter.Group();
            grp_a.set_size(320, 240);
            grp_a.x = grp_a.y = 0;
            grp_a.show();
            stage.add_actor(grp_a);
            grp_m = new ContainerModel(grp_a);

            Clutter.Rectangle rect_a = new Clutter.Rectangle();
            rect_a.set_size(128, 128);
            rect_a.color = white;
            rect_a.border_width = 3;
            rect_a.x = 100;
            rect_a.y = 100;
            rect_a.show();
            rect_m = new ActorModel(rect_a);

            Timeline timeline = new Timeline(120, 60); 
            timeline.set_loop(true);
            timeline.new_frame += frame_callback;
            timeline.start();

            Clutter.main();
        }

        public static void main(string[] args)
        {
            Clutter.init(ref args);
            new Hierarchy();
        }
    }
}

