/*
 *  turbulence/t/bloom.vala
 *
 *  Copyright (c) 2008 Patrick Walton <pcwalton@uchicago.edu>
 */

using GLib;
using Gdk;
using Clutter;
using Cairo;
using Turbulence;

namespace TurbulenceTests {
    public class Bloom : Object {
        construct {
            Clutter.Color blue, white;
            Clutter.Color.parse("blue", out blue);
            Clutter.Color.parse("white", out white);

            Stage stage = (Stage)Stage.get_default();
            stage.color = blue;

            Gdk.Pixbuf pixbuf = new Gdk.Pixbuf.from_file("chrono.jpg");

            Texture tex_a = new Texture.from_pixbuf(pixbuf);
            tex_a.x = 25;
            tex_a.y = 25;
            stage.add_actor(tex_a);

            ActorModel tex_m = new ActorModel(tex_a);
            BloomFilter bloom = new BloomFilter();
            tex_m.filter = bloom;

            stage.show_all();
            Clutter.main();
        }

        public static void main(string[] args)
        {
            Clutter.init(ref args);
            new TurbulenceTests.Bloom();
        }
    }
}

