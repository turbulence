/*
 *  turbulence/t/cairo.vala
 *
 *  Copyright (c) 2008 Patrick Walton <pcwalton@uchicago.edu>
 */

using GLib;
using Clutter;
using Cairo;
using Turbulence;

namespace TurbulenceTests {
    public class Cairo : Object {
        public void draw(CairoSoftwareActor sender, Context ctx)
        {
            ctx.set_source_rgb(1.0, 0.0, 0.0);
            ctx.set_line_width(3);
            ctx.new_path();
            ctx.move_to(63, 63);
            ctx.rel_line_to(2 * 20, 2 * 20);
            ctx.rel_line_to(-2 * 20, 0);
            ctx.rel_line_to(2 * 20, -2 * 20);
            ctx.close_path();
            ctx.stroke();
        }

        construct {
            Clutter.Color blue, white;
            Clutter.Color.parse("blue", out blue);
            Clutter.Color.parse("white", out white);

            Stage stage = (Stage)Stage.get_default();
            stage.color = blue;
            stage.show();

            CairoSoftwareActor csa = new CairoSoftwareActor();
            csa.set_size(128, 128);
            csa.x = 100;
            csa.y = 100;
            csa.draw_in_context += draw;
            stage.add_actor(csa);
            csa.show();

            Clutter.main();
        }

        public static void main(string[] args)
        {
            Clutter.init(ref args);
            new TurbulenceTests.Cairo();
        }
    }
}

