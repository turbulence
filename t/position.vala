/*
 *  turbulence/t/position.vala
 *
 *  Copyright (c) 2008 Patrick Walton <pcwalton@uchicago.edu>
 */

using GLib;
using Gdk;
using Clutter;
using Turbulence;

namespace TurbulenceTests {
    public class Position : Object {
        construct {
            Clutter.Color blue, white;
            Clutter.Color.parse("blue", out blue);
            Clutter.Color.parse("white", out white);

            Stage stage = (Stage)Stage.get_default();
            stage.color = blue;
            stage.show();

            Clutter.Rectangle rect_a = new Clutter.Rectangle();
            rect_a.set_size(128, 128);
            rect_a.color = white;
            rect_a.border_width = 3;
            rect_a.x = 100;
            rect_a.y = 100;
            stage.add_actor(rect_a);
            rect_a.show();

            ActorModel rect_m = new ActorModel(rect_a);

            rect_m.position = new Turbulence.Point(
                    UnitFunctions.pixels_to_units(250),
                    UnitFunctions.pixels_to_units(250));

            Clutter.main();
        }

        public static void main(string[] args)
        {
            Clutter.init(ref args);
            new Position();
        }
    }
}

