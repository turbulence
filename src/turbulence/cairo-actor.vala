/*
 *  turbulence/cairo-actor.vala
 *
 *  Copyright (c) 2008 Patrick Walton <pcwalton@uchicago.edu>
 */

using GLib;
using Clutter;
using CairoTurbulence;

namespace Turbulence {
    public abstract class CairoActor : Actor, IAnimatable {
        public signal void draw_in_context(Context context);

        public abstract Behaviour get_behavior_for_key_path(string kp, Alpha
                alpha, Timeline timeline);
    }
}

