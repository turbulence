/*
 *  turbulence/box.vala
 *
 *  Copyright (c) 2008 Patrick Walton <pcwalton@uchicago.edu>
 */

using GLib;
using Gee;

namespace Turbulence {
    public class Box : Object, ILayoutManager {
        public ActorModel model { get; set; }

        public void invalidate_layout()
        {
            /* empty */
        }

        public Size get_preferred_size_of_model(ActorModel model)
        {
            return null;    /* TODO */
        }

        public void layout_submodels()
        {
            /* TODO */
        }

        public ArrayList<BoxElement> elements { get; private set; }
        construct {
            elements = new ArrayList<BoxElement>();
        }
    }
}

