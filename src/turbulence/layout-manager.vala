/*
 *  turbulence/layout-manager.vala
 *
 *  Copyright (c) 2008 Patrick Walton <pcwalton@uchicago.edu>
 */

using GLib;

namespace Turbulence {
    public interface ILayoutManager {
        public abstract ActorModel model { get; set; }

        /*
         *  Called when the preferred size of the given model might have
         *  changed. The layout manager should flush any cached state it might
         *  have relating to the size of that model.
         */
        public abstract void invalidate_layout();

        /*
         *  Called when the submodels of the given model have changed in some
         *  way. The layout manager is expected to resize the submodels
         *  appropriately.
         */
        public abstract void layout_submodels();

        /*
         *  Computes and returns the preferred size of the given model.
         */
        public abstract Size get_preferred_size_of_model(ActorModel model);
    }
}

