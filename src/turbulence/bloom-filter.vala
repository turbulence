/*
 *  turbulence/bloom-filter.vala
 *
 *  Copyright (c) 2008 Patrick Walton <pcwalton@uchicago.edu>
 *  GLSL filter code by Romain Guy <http://www.curious-creature.org/
 *      2007/02/20/fast-image-processing-with-jogl/>
 */

using GLib;
using Gee;

namespace Turbulence {
    public class BloomFilter : Filter {
        protected override string# glsl_source {
            get {
                return
"uniform sampler2DRect rectTexture; "                                       +
"uniform float brightPassThreshold;"                                        +
"void main(void) {"                                                         +
"    vec3 luminanceVector = vec3(0.2125, 0.7154, 0.0721);"                  +
"    vec4 sample = texture2DRect(rectTexture, gl_TexCoord[0].st);"          +
"    float luminance = dot(luminanceVector, sample.rgb);"                   +
"    luminance = max(0.0, luminance - brightPassThreshold);"                +
"    sample.rgb *= sign(luminance);"                                        +
"    sample.a = 1.0;"                                                       +
"    gl_FragColor = sample;"                                                +
"}"                                                                         ;
            }
        }

        private HashMap<string, weak Float> _glsl_params = null;
        protected override HashMap<string, weak Float> glsl_params {
            get {
                if (_glsl_params == null) {
                    _glsl_params = new HashMap<string, weak Float>(str_hash,
                            str_equal);

                    /*
                     *  Ugh... awful hack. When will Gee support real hash maps
                     *  that manage memory properly? :(
                     */

                    Float *fp = new Float(0.75F);
                    _glsl_params.set("brightPassThreshold", fp);
                }

                return _glsl_params;
            }
        }
    }
}

