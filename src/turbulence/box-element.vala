/*
 *  turbulence/box-element.vala
 *
 *  Copyright (c) 2008 Patrick Walton <pcwalton@uchicago.edu>
 */

using GLib;

namespace Turbulence {
    public abstract class BoxElement : Object {
        public abstract ActorModel model { get; set; }
    }

    public class BoxSpring : BoxElement {
        public ActorModel model { get; set; }

        public BoxSpring(ActorModel m)
        {
            model = m;
        }
    }

    public class BoxStrut : BoxElement {
        public ActorModel model { get; set; }
        public uint length      { get; set; }

        public BoxStrut(ActorModel m, uint l)
        {
            model = m;
            length = l;
        }
    }
}

