/*
 *  turbulence/animation.vala
 *
 *  Copyright (c) 2008 Patrick Walton <pcwalton@uchicago.edu>
 */

using GLib;
using Clutter;

namespace Turbulence {
    public class Animation : Object {
        public Timeline timeline    { get; private set; }
        public Alpha alpha          { get; private set; }
        public Behaviour behavior   { get; private set; }

        public Animation(Timeline t, Alpha a, Behaviour b)
        {
            timeline = t;
            alpha = a;
            behavior = b;
        }
    }
}

