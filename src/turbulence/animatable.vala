/*
 *  turbulence/animatable.vala
 *
 *  Copyright (c) 2008 Patrick Walton <pcwalton@uchicago.edu>
 */

using GLib;
using Clutter;

namespace Turbulence {
    public interface IAnimatable {
        public abstract Behaviour get_behavior_for_key_path(string key_path,
                Alpha alpha, Timeline timeline);
    }
}

