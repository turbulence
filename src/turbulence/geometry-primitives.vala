/*
 *  turbulence/geometry-primitives.vala
 *
 *  Copyright (c) 2008 Patrick Walton <pcwalton@uchicago.edu>
 */

using GLib;
using Clutter;

namespace Turbulence {
    /*
     *  All geometry primitives are immutable. This is done so that objects
     *  can detect the setting of a new geometry primitive.
     */

    public class Point : Object {
        public uint x { get; private set; }
        public uint y { get; private set; }

        public Point(uint in_x, uint in_y)
        {
            x = in_x;
            y = in_y;
        }
    }

    public class Size : Object {
        public uint width   { get; private set; }
        public uint height  { get; private set; }

        public Size(uint in_width, uint in_height)
        {
            width = in_width;
            height = in_height;
        }
    }

    public class Rect : Object {
        public Point origin { get; private set; }
        public Size size    { get; private set; }

        public Rect(uint x, uint y, uint width, uint height)
        {
            origin = new Point(x, y);
            size = new Point(width, height);
        }
    }
}

