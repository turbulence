/*
 *  turbulence/update.vala
 *
 *  Copyright (c) 2008 Patrick Walton <pcwalton@uchicago.edu>
 */

using GLib;

namespace Turbulence {
    public class Update : Object {
        public ActorModel model { get; set; }
        public string key_path { get; set; }

        /* internal */ public Update(ActorModel m, string kp)
        {
            model = m;
            key_path = kp;
        } 
    }
}

