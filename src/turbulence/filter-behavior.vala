/*
 *  turbulence/filter-behavior.vala
 *
 *  Copyright (c) 2008 Patrick Walton <pcwalton@uchicago.edu>
 */

using GLib;
using Clutter;

namespace Turbulence {
    public class FilterBehavior : Behaviour {
        public Filter filter    { get; set; }
        public string param     { get; set; }
        public float from       { get; set; }
        public float to         { get; set; }

        public FilterBehavior(Filter in_filter, string in_param, float in_from,
                float in_to)
        {
            filter  = in_filter;
            param   = in_param;
            from    = in_from;
            to      = in_to;
        }

        public override void alpha_notify(uint alpha_value)
        {
            float val = from + ((float)alpha_value / ALPHA_MAX_ALPHA) *
                (to - from);

            int count = get_n_actors();
            for (int i = 0; i < count; i++)
                get_nth_actor(i).set_shader_param(param, val); 

            /*
             *  The whole "presentation param" botch is necessary because
             *  there's no get_shader_param() method on a ClutterActor... :(
             */
            filter._set_presentation_param(param, val);
        }
    }
}

