/*
 *  turbulence/float.vala
 *
 *  Copyright (c) 2008 Patrick Walton
 */

using GLib;

namespace Turbulence {
    public class Float : Object {
        public float val { get; set; }

        public Float(float n)
        {
            val = n;
        }
    }
}

