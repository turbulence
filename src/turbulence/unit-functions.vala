/*
 *  turbulence/unit-functions.vala
 *
 *  Copyright (c) 2008 Patrick Walton
 */

using GLib;
using Clutter;

namespace Turbulence {
    public static class UnitFunctions {
        public static uint pixels_to_units(int x)
        {
            return int_to_units(x);
        }

        public static int units_to_pixels(uint x)
        {
            return units_to_int(x);
        }

        public static uint fixed_to_units(uint x)
        {
            return (uint)x;
        }

        public static uint int_to_units(int x)
        {
            return (uint)int_to_fixed(x);
        }

        public static int units_to_int(uint x)
        {
            return fixed_to_int((uint)x);
        }

        public static uint float_to_units(float x)
        {
            return (uint)float_to_fixed(x);
        }

        public static float units_to_float(uint x)
        {
            return fixed_to_float((uint)x);
        }

        /*
         *  Fixed point utilities
         */
        public const int Q      = 16;
        public const int ZERO   = 0;
        public const int ONE    = (1 << Q);
        public const int HALF   = 32768;
        public const int MAX    = 0x7fffffff;
        public const int MIN    = 0x80000000;
        public const int PI     = 0x0003243f;
        public const int _2PI   = 0x0006487f;
        public const int PI_2   = 0x00019220;
        public const int PI_4   = 0x0000c910;
        public const int _60    = (60 << Q);
        public const int _120   = (120 << Q);
        public const int _180   = (180 << Q);
        public const int _240   = (240 << Q);
        public const int _255   = (255 << Q);
        public const int _360   = (360 << Q);

        public static int fixed_to_int(uint x)
        {
            return ((int)x >> Q);
        }

        public static uint int_to_fixed(int x)
        {
            return (uint)(x << Q);
        }

        public static float fixed_to_float(uint x)
        {
            return (float)((int)x / 65536.0F);
        }

        public static uint float_to_fixed(float x)
        {
            return (uint)(int)((double)x * (double)ONE);
        }

        public static uint fixed_multiply(uint x, uint y)
        {
            return (x >> 8) * (y >> 8);
        }
    }
}

